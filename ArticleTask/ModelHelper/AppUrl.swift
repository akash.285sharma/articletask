//
//  TPUrl.swift
//  TripPusher
//
//  Created by @mrendra on 14/05/17.
//  Copyright © 2017 Akash Sharma. All rights reserved.
//

import Foundation



struct AppUrl {
    
    static let mainUrl = "https://api.nytimes.com/svc/"
    
    static let ApiKey = "cqALAZhVAWuvRUYji9PDcOvKctlKXiyE"

    
    static let post = "POST"
    
    static let get = "GET"
    
    static let getMostViewd = mainUrl + "mostpopular/v2/viewed/7.json"

    
    static let getMostShared = mainUrl + "mostpopular/v2/shared/1/facebook.json"

    
    static let getMostEmailed = mainUrl + "mostpopular/v2/emailed/1.json"

    
    static let getSearchArticle = mainUrl + "search/v2/articlesearch.json"

    
    

    
}
