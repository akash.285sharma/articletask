//
//  ArticleListModel.swift
//  ArticleTask
//
//  Created by Akash S on 22/07/19.
//  Copyright © 2019 Akash S. All rights reserved.
//

import Foundation

struct  ArticleListModel{
    var headline : String
    var publishdate : String
    
    init(_ dictionary: [String: Any]) {
        self.headline = dictionary["title"] as? String ?? ""
        self.publishdate = dictionary["pub_date"] as? String ?? ""
    }
}


