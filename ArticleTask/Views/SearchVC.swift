//
//  SearchVC.swift
//  ArticleTask
//
//  Created by Akash S on 22/07/19.
//  Copyright © 2019 Akash S. All rights reserved.
//

import UIKit

class SearchVC: UIViewController {

    @IBOutlet weak var txtSearch: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Search"
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnSearchClicked(_ sender: Any)
    {
        let str = txtSearch.text
        let trimmed = str!.trimmingCharacters(in: .whitespacesAndNewlines)
        var data = [[String: Any]]()
        var model = [ArticleListModel]()

        if (txtSearch.text?.isEmpty != true && trimmed.count != 0)
        {
            let parameters = ["q": txtSearch.text!, "api-key": AppUrl.ApiKey] as Dictionary<String,Any>
            
            MBProgressHUD.showAdded(to: self.view, animated: true);
            
            ApiCaller.getData(apiUrl: AppUrl.getSearchArticle, strMethodType: AppUrl.get,params: parameters as! [String : String], completion:{ response in
                
                
                DispatchQueue.main.async() { () -> Void in
                    
                    MBProgressHUD.hide(for: self.view, animated: true);
                    
                    
                    if (response["status"] != nil)
                    {
                        let StatusCode : String = response["status"] as! String
                        
                        if (StatusCode ==  "OK")
                        {
                            let dicResponce = response["response"] as! [String:Any]
                            data = dicResponce["docs"] as! [[String : Any]]
                            
                            
                            for dic in data
                            {
                                
                                if let dicHeadline = dic["headline"] as? [String:Any]
                                {
                                    guard let title = dicHeadline["main"] as? String else {
                                        continue
                                        
                                    }
                                    guard let pubDate = dic["pub_date"] as? String else {
                                        continue
                                        
                                    }
                                    
                                    let val = self.getDate(date: pubDate)
                                    let dicValue = [
                                        "title":title ,"pub_date":val] as [String : Any]
                                    model.append(ArticleListModel(dicValue))
                                    
                                }

                            }
                            
                            if (model.count>0)
                            {
                                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ArticelListTVC") as? ArticelListTVC
                                vc?.arrArticleList = model
                                self.navigationController?.pushViewController(vc!, animated: true)

                            }
                            else
                            {
                                ApiCaller.showAlertView(strAlertText: "No Data Found.")
                            }


                        }
                        
                    }
                    else
                    {
                        ApiCaller.showAlertView(strAlertText: "The Connection timed out.")
                    }
                    
                    
                    
                }
                
                
            })
        }
        else
        {
            ApiCaller.showAlertView(strAlertText: "Please type articel.")

        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SearchVC
{
    func getDate(date: String) -> String
    {
        let dateComponets = date.components(separatedBy: "T")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        let dateFromString : NSDate = dateFormatter.date(from: dateComponets[0])! as NSDate
        dateFormatter.dateFormat = "dd-MMM-yyyy"
        let datenew = dateFormatter.string(from: dateFromString as Date)
        return datenew
    }
}
