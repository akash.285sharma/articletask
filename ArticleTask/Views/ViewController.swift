//
//  ViewController.swift
//  ProfileAkashTest
//
//  Created by Akash S on 22/07/19.
//  Copyright © 2019 Akash S. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tblMain: UITableView!
    var arrPopular = NSArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        arrPopular = ["Most Viewed","Most Shared","Most Emailed"]
        // Do any additional setup after loading the view, typically from a nib.
    }
    

}

extension ViewController:UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if (section == 0)
        {
            return "Search"
        }
        else
        {
            return "Popular"
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if (section == 0)
        {
            return 1

        }
        else
        {
            return arrPopular.count

        }
        
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let vc = tblMain.dequeueReusableCell(withIdentifier: "cellMain", for: indexPath) as! MainTableViewCell
        
        if (indexPath.section == 0)
        {
            vc.lblTitle.text = "Search Articles"
        }
        else
        {
            vc.lblTitle.text = (arrPopular.object(at: indexPath.row) as! String)
        }
        
        return vc
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if (indexPath.section == 0)
        {
            let mapViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "SearchVC") as? SearchVC
            
            self.navigationController?.pushViewController(mapViewControllerObj!, animated: true)

        }
        else
        {
            switch (indexPath.row) {
            case 0:
                self.getArticleLise(strText: AppUrl.getMostViewd)
                
            case 1:
                self.getArticleLise(strText: AppUrl.getMostShared)

            case 2:
               self.getArticleLise(strText: AppUrl.getMostEmailed)

            default:
                return
            }

        }
    }
}

extension ViewController
{
    func getArticleLise(strText:String)
    {
        let parameters = ["api-key": AppUrl.ApiKey] as Dictionary<String,Any>
        var data = [[String: Any]]()
        var model = [ArticleListModel]()
        
         MBProgressHUD.showAdded(to: self.view, animated: true);
        
        ApiCaller.getData(apiUrl: strText, strMethodType: AppUrl.get,params: parameters as! [String : String], completion:{ response in
            
            
            DispatchQueue.main.async() { () -> Void in
                
                   MBProgressHUD.hide(for: self.view, animated: true);
                
                
                if (response["status"] != nil)
                {
                    let StatusCode : String = response["status"] as! String
                    
                    if (StatusCode ==  "OK")
                    {
                        
                        data = response["results"] as! [[String : Any]]
                        print(data)
                        
                        
                        for dic in data
                        {
                            
                                guard let title = dic["title"] as? String else {
                                    continue
                                    
                                }
                                guard let pubDate = dic["published_date"] as? String else {
                                    continue
                                    
                                }
                                let objSearch = SearchVC()
                                let val = objSearch.getDate(date: pubDate)
                                let dicValue = [
                                    "title":title ,"pub_date":val] as [String : Any]
                                model.append(ArticleListModel(dicValue))
                                
                        }
                        
                        if (model.count>0)
                        {
                            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ArticelListTVC") as? ArticelListTVC
                            vc?.arrArticleList = model
                            self.navigationController?.pushViewController(vc!, animated: true)

                        }
                        else
                        {
                             ApiCaller.showAlertView(strAlertText: "No Data Found.")
                        }

                        
                    }
                    
                }
                else
                {
                    ApiCaller.showAlertView(strAlertText: "The Connection timed out.")
                }
                
                
                
            }
            
            
        })
        
    }
}
